# 🍵 [Boba Maps](https://www.bobamaps.xyz) 🥛
[![License:GPL-3.0-only](https://img.shields.io/github/license/RiceAbove/Boba-Maps)](https://opensource.org/licenses/GPL-3.0)

Find bubble tea restaurants in your area!

## About
Boba Maps is a dynamic web application that helps you find bubble tea restaurants in your area! Just simply type in an address in the search bar & the site will quickly find shops in that location. It's desktop & mobile friendly so you can find boba anywhere you go!

## What is bubble tea?
This is the definition according to [Wikipedia](https://en.wikipedia.org/wiki/Bubble_tea):
> Bubble tea is a Taiwanese tea-based drink invented in Tainan and Taichung in the 1980s. Recipes contain tea of some kind, flavors of milk, and sugar. Toppings, known as "pearls", such as chewy tapioca balls, popping boba, fruit jelly, grass jelly, agar jelly, alovera jelly,sago and puddings are often added.

## Built With

### Front End
- HTML5
- CSS3 
- JavaScript
- Bootstrap/Bootstrap Studio

### Back End
- Python
- Django

### API's
- Yelp Fusion
- Google Maps JavaScript 
- SendGrid

### Cloud Platform
- Heroku

## License
This project is licensed under the [GPL-3.0](LICENSE) - see the LICENSE file for details

## Contributing Guide
Please read [CONTRIBUTING.md](CONTRIBUTING.md), [CODE_OF_CONDUCT.md](CODE_OF_CONDUCT.md), & [PULL_REQUEST_TEMPLATE.md](.github/ISSUE_TEMPLATE/PULL_REQUEST_TEMPLATE.md) on the process for submitting pull requests.

## Contributors 
- [**Gico Carlo Evangelista**](https://github.com/RiceAbove) - Creator, Full Stack 

## Acknowledgements
My girlfriend and I love boba, so I decided to make this fun project over to the summer so that we could quickly find bubble tea shops wherever we go. It's not intended to replace Yelp at all. This being my first web application, it still has a lot of work to be done to polish and scale it in the future. 

## Donation
This web app is completely free to use! It's not necessary, but you could always support me by buying me a coffee :-)

<a href="https://www.buymeacoffee.com/gico" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: auto !important;width: auto !important;" ></a>
